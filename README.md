# ToDo-List-App-CLI

This is a CLI based to-do list app that was given as a programming problem by GDC Network. I created the same program in 10 different programming languages.

GDC network also provided test file to test the app. There's 16 test cases in total. I ran the same test for each programming language and recorded the execution time.
(I tried to keep the algorithm pretty much the same in every language. Of course I made sure all test cases have passed.)

The result was as follows :-

| Language   | Execution time (s) `min` | Execution time (s) `max` |
| :------------- |:-------------:| :------------: |
| C | 1.202 | 1.709 |
| C++ | 1.260 | 2.080 |
| C# | 4.144 | 5.068 |
| Dart | 1.914 | 7.336 |
| Go | 1.313 | 2.238 |
| Java | 8.204 | 8.716 |
| Javascript | 4.583 | 5.096 |
| Kotlin | 8.007 | 8.313 |
| Python | 3.363 | 7.707 |
| Ruby | 7.486 | 8.444 |
| Swift | 1.716 | 2.311 |

Time taken by each language was recorded 3 times. Here's the min and max values for comparison.

**Highest score:**
1.202 seconds by `C`

**Lowest score:**
8.716 seconds by `Java`

While testing some cases manually, I observed that among compiler based OOPs, Kotlin compiler was really slow while building `jar` file. I might be wrong though.

I found `C#` and `C++` quite easy for file handling.

(Btw the usage of programming languages shown by github in this repository has javascript with maximum value. It's because all tests were written in it)

Planning to write the same program in assembly :)